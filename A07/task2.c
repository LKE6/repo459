#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mvmul.h"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: ./task2 n\n");
        return 1;
    }

    int n = atoi(argv[1]);

    double *A = malloc(n * n * sizeof(double));
    double *b = malloc(n * sizeof(double));
    double *c = malloc(n * sizeof(double));

    srand(time(NULL));

    for (int i = 0; i < n * n; i++) {
        A[i] = ((double)rand() / RAND_MAX) * 2 - 1; // Random number in [-1, 1]
    }

    for (int i = 0; i < n; i++) {
        b[i] = 1.0;
        c[i] = 0.0;
    }

    clock_t start_time = clock();
    mvmul(A, b, c, n);
    clock_t end_time = clock();
    double elapsed_time = ((double) (end_time - start_time) / CLOCKS_PER_SEC) * 1000.0;

    printf("%lf\n%lf\n", c[n-1], elapsed_time);

    free(A);
    free(b);
    free(c);

    return 0;
}
