#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "sort.h"

#define LENGTH 15

int main(void) {
    int A[LENGTH];
    srand(time(NULL));
    for (int i = 0; i < LENGTH; i++) {
        A[i] = rand() % 100;
        printf("%d ", A[i]);
    }
    printf("\n");
    sort(A, LENGTH);
    for (int i = 0; i < LENGTH; i++) {
        printf("%d ", A[i]);
    }
    printf("\n");
    return 0;
}
