#ifndef SORT_H
#define SORT_H

#include <stdlib.h>

void sort(int* A, size_t n);

#endif
