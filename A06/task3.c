#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>

// Comparison function for qsort
int compare_ints(const void *a, const void *b) {
    int int_a = *((int*) a);
    int int_b = *((int*) b);

    if (int_a == int_b)
        return 0;
    else if (int_a < int_b)
        return 1;
    else
        return -1;
}

int main(int argc, char *argv[]) {
    // Ensure that the correct number of command line arguments was provided
    if (argc != 2) {
        printf("Usage: %s N\n", argv[0]);
        return 1;
    }

    // Convert the input argument to an integer
    int N = 0;
    for (int i = 0; argv[1][i] != '\0'; i++) {
        if (!isdigit(argv[1][i])) {
            printf("Invalid input: %s\n", argv[1]);
            return 1;
        }
        int digit = argv[1][i] - '0';
        if (N > INT_MAX/10 || (N == INT_MAX/10 && digit > INT_MAX%10)) {
            printf("Input value is too large.\n");
            return 1;
        }
        N = N * 10 + digit;
    }

    // Ensure that the input is a positive integer
    if (N <= 0) {
        printf("Input must be a positive integer.\n");
        return 1;
    }

    // Allocate memory for the array
    int *arr = malloc((N + 1) * sizeof(int));
    if (arr == NULL) {
        printf("Memory allocation error: ");
        perror("");
        return 1;
    }

    // Read in the input values
    for (int i = 0; i <= N; i++) {
        int ret = scanf("%d", &arr[i]);
        if (ret != 1) {
            printf("Error reading input.\n");
            free(arr);
            return 1;
        }
    }

    // Sort the array in descending order
    qsort(arr, N+1, sizeof(int), compare_ints);

    // Print the sorted array
    for (int i = 0; i <= N; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");

    // Free the memory used by the array
    free(arr);
    return 0;
}
