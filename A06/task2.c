#include <stdio.h>
#include <stdlib.h>
#include "structs.h"

int main() {
    printf("Size of struct A: %ld bytes\n", sizeof(struct A));
    printf("Size of struct B: %ld bytes\n", sizeof(struct B));

    // Allocate memory for struct A on the heap
    struct A* a_ptr = (struct A*)malloc(sizeof(struct A));

    // Set the fields of the struct
    a_ptr->i = 42;
    a_ptr->c = 'x';
    a_ptr->d = 3.14159;

    // Print each field of the struct
    printf("Field i: %d\n", a_ptr->i);
    printf("Field c: %c\n", a_ptr->c);
    printf("Field d: %lf\n", a_ptr->d);

    // Free the memory allocated for the struct
    free(a_ptr);
    return 0;
}

gcc task2.c -Wall -O3 -o task2


./task2



The sizes of the two structs A and B output by the program are different because their field declarations are in different order.

In struct A, the fields are declared in the order int, char, and double, which means that the size of struct A is determined by the sizes of its individual fields and any padding bytes that may be added by the compiler for alignment purposes.

In struct B, the fields are declared in a different order: int, double, and char. This means that the size of struct B is also determined by the sizes of its fields and any padding bytes added for alignment. However, because the order of the fields is different, the padding added between fields may be different, leading to a different overall size for struct B.

In general, the size of a struct in C is determined by the sizes of its fields and any padding added by the compiler to ensure proper alignment of the fields in memory. The amount of padding added may vary depending on the size and types of the fields and the requirements of the target platform.
