#!/usr/bin/env python
# coding: utf-8

# In[14]:


def matmul(A, B):
    n = len(A)
    C = [[0 for j in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            for k in range(n):
                C[i][j] += A[i][k] * B[k][j]
    return C


# In[16]:




# Define two matrices A and B
A = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
B = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]

# Compute the matrix product C
C = matmul(A, B)

# Print the result
for row in C:
    print(row)


# In[ ]:





# In[7]:


# Generate random matrices A and B
n_values = [2**7, 2**8, 2**9]


# In[8]:


for n in n_values:
    A = [[random.randint(0, 9) for j in range(n)] for i in range(n)]
    B = [[random.randint(0, 9) for j in range(n)] for i in range(n)]


# In[17]:


import random
import time


# Generate random matrices A and B
n_values = [2**7, 2**8, 2**9]
for n in n_values:
    A = [[random.randint(0, 9) for j in range(n)] for i in range(n)]
    B = [[random.randint(0, 9) for j in range(n)] for i in range(n)]
    # Time the matmul function
    start_time = time.perf_counter()

    end_time = time.perf_counter()
    # Print the time taken for each case
    print("n = {}: {:.6f} seconds".format(n, end_time - start_time))
    # Print the resulting matrix C to verify correctness
    print("Resulting matrix C:")
    for row in C:
        print(row)


# In[ ]:




