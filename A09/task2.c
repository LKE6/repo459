#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void test(long *data, long elems, long stride)
{
    volatile long accum = 0;
    long i, sx2 = stride * 2, sx3 = stride * 3, sx4 = stride * 4;
    long acc0 = 0, acc1 = 0, acc2 = 0, acc3 = 0;
    long length = elems;
    long limit = length - sx4;
    /* Combine 4 elements at a time */
    for (i = 0; i < limit; i += sx4) {
        acc0 = acc0 + data[i];
        acc1 = acc1 + data[i + stride];
        acc2 = acc2 + data[i + sx2];
        acc3 = acc3 + data[i + sx3];
    }
    /* Finish any remaining elements */
    for (; i < length; i += stride) {
        acc0 = acc0 + data[i];
    }
    accum = ((acc0 + acc1) + (acc2 + acc3));
}

void fill_data(long *data, long elems)
{
    long i;
    for (i = 0; i < elems; i++) {
        double val = ((double) rand() / (double) RAND_MAX) * 2 - 1;
        data[i] = (long)(val * 1000000);
    }
}

int main()
{
    long n_elems = 512 * 1024 / sizeof(long);
    long *data = (long*) malloc(n_elems * sizeof(long));
    fill_data(data, n_elems);

    printf("Timing for test1\n");
    double avg_time_ms;
    struct timespec start, end;
    int i, j;

    for (i = 1; i <= 17; i = (i < 4) ? (i+1) : (i+3)) {
        avg_time_ms = 0;
        for (j = 0; j < 100; j++) {
            clock_gettime(CLOCK_REALTIME, &start);
            test(data, n_elems, i);
            clock_gettime(CLOCK_REALTIME, &end);
            double elapsed_ms = (end.tv_sec - start.tv_sec) * 1000.0 + (end.tv_nsec - start.tv_nsec) / 1000000.0;
            avg_time_ms += elapsed_ms;
        }
        avg_time_ms /= 100.0;
        printf("%.2f ", avg_time_ms);
    }
    printf("\n");

    printf("Timing for test2\n");
    volatile long accum = 0;
    for (i = 1; i <= 17; i = (i < 4) ? (i+1) : (i+3)) {
        avg_time_ms = 0;
        for (j = 0; j < 100; j++) {
            clock_gettime(CLOCK_REALTIME, &start);
            test(data, n_elems, i);
            accum = accum + volatile long(accum);
            clock_gettime(CLOCK_REALTIME, &end);
            double elapsed_ms = (end.tv_sec - start.tv_sec) * 1000.0 + (end.tv_nsec - start.tv_nsec) / 1000000.0;
            avg_time_ms += elapsed_ms;
        }
        avg_time_ms /= 100.0;
        printf("%.2f ", avg_time_ms);
    }
    return 0;
}
