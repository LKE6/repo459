#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "matmul.h"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s n\n", argv[0]);
        return 1;
    }

    const size_t n = (size_t) atoi(argv[1]);

    // Allocate memory for matrices A, B, and C
    double *A = malloc(n * n * sizeof(double));
    double *B = malloc(n * n * sizeof(double));
    double *C = malloc(n * n * sizeof(double));

    // Populate matrices A and B with random values between -1 and 1
    srand(time(NULL));
    for (size_t i = 0; i < n * n; i++) {
        A[i] = ((double)rand() / (double)RAND_MAX) * 2.0 - 1.0;
        B[i] = ((double)rand() / (double)RAND_MAX) * 2.0 - 1.0;
    }

    // Call mmul1 and time the execution
    clock_t start = clock();
    mmul1(A, B, C, n);
    clock_t end = clock();
    double time_ms = (double)(end - start) * 1000.0 / CLOCKS_PER_SEC;

    // Output timing results and last element of C
    printf("%.1f\n", time_ms);
    printf("%.6f\n", C[n * n - 1]);

    // Free memory
    free(A);
    free(B);
    free(C);

    return 0;
}

 #The corrected mmul1 function in matmul.c is:

void mmul1(const double* A, const double* B, double* C, const size_t n) {
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < n; ++j) {
            C[i * n + j] = 0.0;
            for (size_t k = 0; k < n; ++k) {
                C[i * n + j] += A[i * n + k] * B[k * n + j];
            }
        }
    }
}
