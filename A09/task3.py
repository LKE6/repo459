import random
import time

# Initialize matrices A and B with random numbers
A = [[random.uniform(-1, 1) for j in range(1400)] for i in range(3400)]
B = [[random.uniform(-1, 1) for j in range(1400)] for i in range(3400)]

# Print out the last entry of A and B
print("Last entry of A: ", A[-1][-1])
print("Last entry of B: ", B[-1][-1])

# Swap each entry of the two matrices row by row using double for loops and time the process
start_time = time.time()
for i in range(3400):
    for j in range(1400):
        temp = A[i][j]
        A[i][j] = B[i][j]
        B[i][j] = temp
end_time = time.time()

# Print out the last entry of A and B
print("Last entry of A: ", A[-1][-1])
print("Last entry of B: ", B[-1][-1])

# Print the timing result in milliseconds
print("Time taken for row-wise swapping: ", (end_time - start_time) * 1000, "ms")

# Swap each entry of the two matrices column by column using double for loops and time the process
start_time = time.time()
for j in range(1400):
    for i in range(3400):
        temp = A[i][j]
        A[i][j] = B[i][j]
        B[i][j] = temp
end_time = time.time()

# Print out the last entry of A and B
print("Last entry of A: ", A[-1][-1])
print("Last entry of B: ", B[-1][-1])

# Print the timing result in milliseconds
print("Time taken for column-wise swapping: ", (end_time - start_time) * 1000, "ms")