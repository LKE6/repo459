#!/usr/bin/env python
# coding: utf-8

# In[10]:


import random
import time


# Define the values of n to test
n_values = [10**3, 10**4, 10**5, 10**6]

for n in n_values:
    # Generate a list A with n random elements
    A = [random.randint(0, 1000) for i in range(n)]
    
    # Print the first element of A before sorting
    print(f"n = {n}, first element of A before sorting: {A[0]}")
    
    # Time the sorter function
    start_time = time.perf_counter()

    end_time = time.perf_counter()
    
    # Print the time taken and the first element of A after sorting
    print(f"n = {n}, time taken to sort A: {end_time - start_time:.6f} seconds")
    print(f"n = {n}, first element of A after sorting: {A[0]}")


# In[ ]:




