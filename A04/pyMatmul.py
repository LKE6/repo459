#!/usr/bin/env python
# coding: utf-8

# In[1]:


def matmul(A, B):
    n = len(A)
    C = [[0 for j in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            for k in range(n):
                C[i][j] += A[i][k] * B[k][j]
    return C


# In[3]:




# Define two matrices A and B
A = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
B = [[9, 8, 7], [6, 5, 4], [3, 2, 1]]

# Compute the matrix product C
C = matmul(A, B)

# Print the result
for row in C:
    print(row)


# In[ ]:




