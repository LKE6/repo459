#!/usr/bin/env python
# coding: utf-8

# In[1]:


def middle(a):
    return a[1:-1]


# In[2]:


a = [1, 2, 3, 4, 5]
result = middle(a)
print(result)  # Output: [2, 3, 4]


# In[ ]:




