#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "sumArray.h"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s n\n", argv[0]);
        return 1;
    }

    int n = atoi(argv[1]);
    if (n < 1 || n > 4096) {
        fprintf(stderr, "Error: n must be between 1 and 4096\n");
        return 1;
    }

    double *A = (double *)malloc(n * n * sizeof(double));
    if (A == NULL) {
        fprintf(stderr, "Error: failed to allocate memory\n");
        return 1;
    }

    // Fill matrix A with random values in the range [-1, 1]
    srand(time(NULL));
    for (int i = 0; i < n * n; i++) {
        A[i] = ((double)rand() / (double)RAND_MAX) * 2.0 - 1.0;
    }

    // Compute sum using sumArray1 and time it
    struct timespec start1, end1;
    clock_gettime(CLOCK_MONOTONIC, &start1);
    double sum1 = sumArray1(A, n);
    clock_gettime(CLOCK_MONOTONIC, &end1);
    double time1 = (end1.tv_sec - start1.tv_sec) * 1000.0 + (end1.tv_nsec - start1.tv_nsec) / 1000000.0;

    // Compute sum using sumArray2 and time it
    struct timespec start2, end2;
    clock_gettime(CLOCK_MONOTONIC, &start2);
    double sum2 = sumArray2(A, n);
    clock_gettime(CLOCK_MONOTONIC, &end2);
    double time2 = (end2.tv_sec - start2.tv_sec) * 1000.0 + (end2.tv_nsec - start2.tv_nsec) / 1000000.0;

    printf("sumArray1:\n");
    printf("Sum = %f\n", sum1);
    printf("Time = %f ms\n", time1);
    printf("\n");

    printf("sumArray2:\n");
    printf("Sum = %f\n", sum2);
    printf("Time = %f ms\n", time2);
    printf("\n");

    free(A);
    return 0;
}

