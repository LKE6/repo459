#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "matmul.h"

#define N 1024
#define RANGE 2.0

double* allocate_matrix(const size_t n) {
    double* mat = malloc(n * n * sizeof(double));
    if (mat == NULL) {
        fprintf(stderr, "Error: Failed to allocate memory for matrix\n");
        exit(EXIT_FAILURE);
    }
    return mat;
}

void fill_matrix(double* mat, const size_t n) {
    srand(time(NULL));
    for (size_t i = 0; i < n * n; i++) {
        mat[i] = (double) rand() / RAND_MAX * RANGE - RANGE/2;
    }
}

void print_last_element(const double* mat, const size_t n) {
    printf("%.1lf\n", mat[n * n - 1]);
}

void test_mmul(const char* label, void (*mmul)(const double*, const double*, double*, const size_t), 
    const double* A, const double* B, double* C, const size_t n) {
    clock_t start = clock();
    mmul(A, B, C, n);
    clock_t end = clock();
    double elapsed_time = (double) (end - start) / CLOCKS_PER_SEC * 1000;
    printf("%s\n%.1lf\n", label, elapsed_time);
    print_last_element(C, n);
}

int main() {
    double* A = allocate_matrix(N);
    double* B = allocate_matrix(N);
    double* C = allocate_matrix(N);
    fill_matrix(A, N);
    fill_matrix(B, N);

    // Test each mmul function
    test_mmul("t1", mmul1, A, B, C, N);
    test_mmul("t2", mmul2, A, B, C, N);
    test_mmul("t3", mmul3, A, B, C, N);
    test_mmul("t4", mmul4, A, B, C, N);

    free(A);
    free(B);
    free(C);
    return 0;
}
