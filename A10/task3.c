#include <stdio.h>
#include <math.h>

#define MAX_ITERATIONS 1000000

float g(float x) {
    return sin(x) / x;
}

int main() {
    float a = 0.0, b = 1.0;
    float xf = 0.5 * (a + b);
    int i = 0;
    
    while (i < MAX_ITERATIONS && b - a > 1e-6) {
        if (g(xf) < 1.0) {
            a = xf;
        } else {
            b = xf;
        }
        xf = 0.5 * (a + b);
        i++;
    }
    
    printf("%.8f\n", xf);
    
    return 0;
}

$ gcc task3.c -Wall -O3 -lm -o task3
$ ./task3
0.99999934