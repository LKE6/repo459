#include <stdio.h>
#include <limits.h>

int main() {
    int a;

    // a) Set variable int a to the greatest number
    a = INT_MAX;
    printf("a (after step a): %d\n", a);

    // b) Add 1 to a
    a += 1;
    printf("a (after step b): %d\n", a);

    // c) Set a to the least (most negative) number
    a = INT_MIN;
    printf("a (after step c): %d\n", a);

    // d) Subtract 1 from a
    a -= 1;
    printf("a (after step d): %d\n", a);

    return 0;
}

#Compile the program using the provided command:
gcc task1.c -Wall -g -o task1


4.#run the program
5../task1

