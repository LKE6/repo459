#include "math_utils.h"
#include <math.h>

// Returns the sum of all entries of arr.
float Sum(const float* arr, size_t count) {
    float sum = 0.0f;
    for (size_t i = 0; i < count; i++) {
        sum += arr[i];
    }
    return sum;
}

// Returns the sum of the sine of each entry of arr.
float SumSines(const float* arr, size_t count) {
    float sum_sines = 0.0f;
    for (size_t i = 0; i < count; i++) {
        sum_sines += sinf(arr[i]);
    }
    return sum_sines;
}

// Returns the sum of entries of arr in the value pointed to by sum, and
// returns the sum of the sines of the entries of arr the value pointed to by sum_sines.
void Fusion(const float* arr, size_t count, float* sum, float* sum_sines) {
    *sum = 0.0f;
    *sum_sines = 0.0f;
    for (size_t i = 0; i < count; i++) {
        *sum += arr[i];
        *sum_sines += sinf(arr[i]);
    }
}

