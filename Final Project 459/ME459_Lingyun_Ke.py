#!/usr/bin/env python
# coding: utf-8

# # Final Project459-Lingyun Ke

# ## Import libraries

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
import math

import sklearn.linear_model 
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
from sklearn import preprocessing
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import DBSCAN


# In[2]:


import warnings
warnings.filterwarnings('ignore')


# ## Loading Dataset

# In[3]:


house_data = pd.read_csv("house data.csv")
house_data


# In[4]:


data = pd.DataFrame(house_data)
data


# ## Exploratory Data Analysis

# In[5]:


data.head()


# In[6]:


data.shape


# In[7]:


data.info()


# In[8]:


data['price']     = data['price'].astype('int64')
data['bedrooms']  = data['bedrooms'].astype('int64')
data['bathrooms'] = data['bathrooms'].astype('int64')
data['floors']    = data['floors'].astype('int64')
data['street']    = data['street'].astype('string')
data['city']      = data['city'].astype('string')
data['statezip']  = data['statezip'].astype('string')
data['country']   = data['country'].astype('string')


# In[9]:


data.info()


# In[10]:


data.drop_duplicates()


# In[11]:


data.describe().T


# In[12]:


(data.price == 0).sum()


# In[13]:


data['price'].replace(0, np.nan, inplace = True)


# In[14]:


data.isnull().sum()


# In[15]:


data.dropna(inplace=True)
(data.price == 0).sum()


# In[16]:


data.describe().T


# # 

# In[17]:


data['date'] = pd.to_datetime(data['date'])
data


# In[18]:


data.insert(1, "year", data.date.dt.year)
data.head()


# In[19]:


data.nunique(axis = 0)  


# In[20]:


data.corr()


# In[21]:


# draw Heat Map 

plt.figure(figsize=(15, 10))
sns.heatmap(data.corr(), annot=True)
plt.title('Heat Map', size=20)
plt.yticks(rotation = 0)
plt.show()


# # 

# # Plots

# In[22]:


plt.figure(figsize=(30,10))

plt.subplot(1,2,1)
plt.title('House Price Distribution Plot')
sns.distplot(data["price"])

plt.subplot(1,2,2)
plt.title('House Price Spread')
sns.boxplot(y=data["price"])

plt.show()


# In[23]:


# make function for count plot and scatter plots for Categorical features

def Categorical_Plot(column,rotation=0):
    
        plt.figure(figsize=(30, 10))
        plt.subplot(1, 2, 1)
        pd.value_counts(data[column]).plot(kind='bar')
        plt.xticks( horizontalalignment="center", fontsize=15, rotation = rotation )
        plt.xlabel(f"\n{str(column)}", fontsize=15)
        plt.ylabel("Count\n", fontsize=15)
        plt.yticks(fontsize = 15)
        plt.title(f"{str(column)} - Count\n", fontsize = 15)

        plt.subplot(1, 2, 2)
        sns.barplot(x = data[column], y = data.price)
        plt.xticks( horizontalalignment="center", fontsize=15, rotation = rotation )
        plt.xlabel(f"\n{str(column)}", fontsize=15)
        plt.yticks(fontsize = 15)
        plt.title(f"{str(column)} - Price\n", fontsize = 15)
        plt.ylabel("Price\n", fontsize=15)

        plt.show()
        print()


# In[24]:


# make function for box plots and scatter plots for Numerical features

def Numerical_Plot(column):
    
        plt.figure(figsize=(20, 5))
        plt.subplot(1,2,1)
        plt.grid(color='black', linestyle='-', linewidth=0.25)
        sns.boxplot(data[column])
        plt.xticks( horizontalalignment="center", fontsize=15)
        plt.xlabel(f"\n{str(column)}", fontsize=15)
        plt.yticks(fontsize = 15)
        plt.title(f"\n{str(column)} Box Plot\n", fontsize = 15)

        plt.subplot(1, 2,2)
        plt.grid(color='black', linestyle='-', linewidth=0.25)
        sns.scatterplot(x=data[column],y=data["price"],hue=data[column])
        plt.xticks( horizontalalignment="center", fontsize=15)
        plt.xlabel(f"\n{str(column)}", fontsize=15)
        plt.yticks(fontsize = 15)
        plt.title(f"{str(column)} - Price\n", fontsize = 15)
        plt.ylabel("Price\n", fontsize=15)

        plt.show()
        print()
        


# In[25]:


Categorical_Plot("bedrooms")


# In[26]:


Categorical_Plot("bathrooms")


# In[27]:


Categorical_Plot("floors")


# In[28]:


Categorical_Plot("waterfront")


# In[29]:


Categorical_Plot("view")


# In[30]:


Categorical_Plot("condition")


# In[31]:


plt.figure(figsize=(15, 5))
sns.barplot(x = data['city'], y = data.price)
plt.xticks( horizontalalignment="center",rotation = 90 )
plt.xlabel("City")
plt.title("City - Price")
plt.ylabel("Price\n")

plt.show()


# In[32]:


Numerical_Plot("sqft_living")


# In[33]:


Numerical_Plot("sqft_lot")


# In[34]:


Numerical_Plot("sqft_above")


# In[35]:


Numerical_Plot("sqft_basement")


# In[36]:


plt.figure(figsize=(25, 5))
sns.pairplot(data)
plt.show()


# In[37]:


data.hist(figsize=(20,30));


# # 

# In[38]:


house = pd.get_dummies(data, columns=['city'], prefix = ['city'])


# In[39]:


house = house.drop(['date', 'street', 'statezip', 'country','sqft_above'], axis = 1)
house


# # 

# # Normalize

# In[40]:


columns = house.columns


# In[41]:


scaler = preprocessing.MinMaxScaler(feature_range = (0, 1))
normal = pd.DataFrame(scaler.fit_transform(house), columns = columns)
normal.head(20)


# In[42]:


house.shape


# # 

# # Modeling

# In[43]:


x = normal.drop("price", axis=1)
y = pd.DataFrame(normal["price"])


# In[44]:


nbrs = NearestNeighbors(n_neighbors=3).fit(x)
neigh_dist, neigh_ind = nbrs.kneighbors(x)
sort_neigh_dist = np.sort(neigh_dist, axis=0)


# In[45]:


k_dist = sort_neigh_dist[:, 2]
plt.plot(k_dist)
plt.ylabel("k-NN distance")
plt.xlabel("Sorted observations (4th NN)")
plt.show()


# In[46]:


clusters = DBSCAN(eps=0.9, min_samples=3).fit(x)
clusters.labels_


# In[47]:


x["noise"] = clusters.labels_
y["noise"] = clusters.labels_


# In[48]:


x = x[x.noise>-1]
y = y[y.noise>-1]
x.drop('noise', inplace = True, axis=1)
y.drop('noise', inplace = True, axis=1)


# In[49]:


X_train, X_test, Y_train, Y_test = train_test_split(x, y, test_size = 0.2, random_state =43 )


# In[50]:


print(X_train.shape)
print(Y_train.shape)
print(X_test.shape)
print(Y_test.shape)


# In[51]:


model = LinearRegression()
model.fit(X_train, Y_train)

Y_pred = model.predict(X_test)


# In[52]:


model.intercept_


# In[53]:


model.coef_


# In[54]:


# Evalutions

# MAE
print("Mean Absolute Error     : ", metrics.mean_absolute_error(Y_test, Y_pred))

# MSE
print("Mean Squared Error      : ", metrics.mean_squared_error(Y_test, Y_pred))

# RMSE
print("Root Mean Squared Error : ", np.sqrt(metrics.mean_squared_error(Y_test, Y_pred)))

#R2 Score
print("R2 Score                : ", metrics.r2_score(Y_test, Y_pred))


# # 
