#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#2-a


# In[1]:


from pySort import sorter
import numpy as np
import sys


# In[ ]:





# In[2]:


#2-b


# In[3]:


import sys

if len(sys.argv) > 1 and sys.argv[1] == '1':
    plotBool = True
else:
    plotBool = False


# In[4]:


from pySort import sorter
import numpy as np
import sys
import time
import matplotlib.pyplot as plt

# Set plotBool based on command line argument
if len(sys.argv) > 1 and sys.argv[1] == '1':
    plotBool = True
else:
    plotBool = False

# Generate random list of integers
A = np.random.randint(0, 1000, size=10000).tolist()

# Time sorter function
start_time = time.time()
sorter(A)
sorter_time = time.time() - start_time

# Time sorted function
start_time = time.time()
sorted(A)
sorted_time = time.time() - start_time

# Print times
print(f"Sorter time: {sorter_time:.3f} seconds")
print(f"Sorted time: {sorted_time:.3f} seconds")

# Plot results if plotBool is True
if plotBool:
    x = ['sorter', 'sorted']
    y = [sorter_time, sorted_time]
    plt.bar(x, y)
    plt.ylabel('Time (seconds)')
    plt.title('Sorting Performance')
    plt.show()


# In[ ]:





# In[5]:


#2-c


# In[6]:


import random
import copy
import numpy as np

n = 10

A = [random.randint(-10, 10) for i in range(n)]
B = copy.deepcopy(A)
A_np = np.array(A)


# In[ ]:





# In[7]:


#2-d


# In[8]:


import numpy as np
import copy
import time

from pySort import sorter
import sys

# Set plotBool based on command line argument
plotBool = False
if len(sys.argv) > 1:
    if sys.argv[1] == '1':
        plotBool = True

# Create lists and numpy array
n = 2**10
A = np.random.randint(-10, 11, size=n).tolist()
B = copy.deepcopy(A)
A_np = np.array(A)

# Measure time for different n values
for i in range(10, 15):
    n = 2**i
    A = np.random.randint(-10, 11, size=n).tolist()
    B = copy.deepcopy(A)
    A_np = np.array(A)

    # Time sorter function
    start_time = time.perf_counter()
    sorter(A)
    sorter_time = time.perf_counter() - start_time

    # Print time taken
    print(f"n = {n}: {sorter_time * 1000:.2f} ms")


# In[ ]:





# In[9]:


import time

# Create list B
B = A.copy()

# Measure time taken for sorting B using Python's sorting function
start_time = time.perf_counter()
B.sort()
sort_time = (time.perf_counter() - start_time) * 1000

print(f"Time taken for sorting B: {sort_time:.6f} ms")


# In[ ]:





# In[10]:


import numpy as np
import time

n = 2**10
A = np.random.randint(-10, 10, size=n)
B = list(A)

# Sorting using numpy's sorting function
start_time = time.perf_counter()
A_sorted = np.sort(A)
numpy_sort_time = (time.perf_counter() - start_time) * 1000

print(f"Time taken to sort numpy array of size {n}: {numpy_sort_time:.6f} ms")


# In[ ]:





# In[11]:


#2-e


# In[ ]:


import numpy as np
import pySort
import sys
import time

# Take command line argument of either 0 or 1 and set plotBool to True if input is 1 and False if input is 0
if len(sys.argv) == 2:
    if sys.argv[1] == '1':
        plotBool = True
    elif sys.argv[1] == '0':
        plotBool = False
    else:
        print("Invalid input argument. Please enter 0 or 1.")
        sys.exit(0)
else:
    print("Please enter a command line argument of either 0 or 1.")
    sys.exit(0)

# Create list A with n elements filled with random integers between -10 and 10
n = 2**14
A = [np.random.randint(-10, 11) for i in range(n)]

# Create list B as a deep copy of A and numpy array A with same elements and size as A
B = A.copy()
arr_A = np.array(A)

# i. Sorting A in-place using sorter function
start_time = time.perf_counter()
pySort.sorter(A)
sorter_time = time.perf_counter() - start_time
print(f"First element of A sorted by sorter function: {A[0]}, time taken: {sorter_time*1000:.4f} ms")

# iii. Sorting numpy array A using numpy's sorting function
start_time = time.perf_counter()
sorted_arr_A = np.sort(arr_A)
numpy_sort_time = time.perf_counter() - start_time
print(f"First element of A sorted by numpy sort function: {sorted_arr_A[0]}, time taken: {numpy_sort_time*1000:.4f} ms")

# Printing the first element of B is not necessary as it has not been sorted
# Sorting B in-place using Python's sorting function
start_time = time.perf_counter()
B.sort()
python_sort_time = time.perf_counter() - start_time
print(f"First element of A sorted by Python sort function: {B[0]}, time taken: {python_sort_time*1000:.4f} ms")


# In[ ]:





# In[13]:


#2-f


# In[28]:


import pySort
import numpy as np
import time
import matplotlib.pyplot as plt

# list of n values
n_values = [2**10, 2**11, 2**12, 2**13, 2**14]

# create empty lists to store times for each sorting method
sorter_times = []
python_sort_times = []
numpy_sort_times = []

# loop through n values
for n in n_values:

    # create A list and B list
    A = [np.random.randint(-10, 11) for _ in range(n)]
    B = A.copy()

    # create numpy array A
    np.random.seed(0)
    A_np = np.random.randint(-10, 11, n)

    # Time sorter function
    start_time = time.perf_counter()
    pySort.sorter(A)
    sorter_times.append(time.perf_counter() - start_time)

    # Time Python's sorting function
    start_time = time.perf_counter()
    B.sort()
    python_sort_times.append(time.perf_counter() - start_time)

    # Time numpy's sorting function
    start_time = time.perf_counter()
    np.sort(A_np)
    numpy_sort_times.append(time.perf_counter() - start_time)

# print first element of A list, B list, and A_np along with the time taken
print(f"sorter: A[0]={A[0]}, time={sorter_times[-1]*1000:.4f} ms")
print(f"python sort: B[0]={B[0]}, time={python_sort_times[-1]*1000:.4f} ms")
print(f"numpy sort: A_np[0]={A_np[0]}, time={numpy_sort_times[-1]*1000:.4f} ms")

# create log-log plot if plotBool is True
plotBool = True
if plotBool:
    # list of times for each sorting method
    sorter_times = [0.001 * t for t in sorter_times]
    python_sort_times = [0.001 * t for t in python_sort_times]
    numpy_sort_times = [0.001 * t for t in numpy_sort_times]

    # create log-log plot
    plt.loglog(n_values, sorter_times, label='sorter')
    plt.loglog(n_values, python_sort_times, label='python sort')
    plt.loglog(n_values, numpy_sort_times, label='numpy sort')

    # add labels and title
    plt.xlabel('n (log base 2)')
    plt.ylabel('Time Taken (ms) (log base 10)')
    plt.title('Comparison of Sorting Algorithms')

    # add legend
    plt.legend()

    # show plot
    plt.show()


# In[ ]:





# In[ ]:





# In[ ]:




