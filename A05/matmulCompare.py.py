#!/usr/bin/env python
# coding: utf-8

# In[ ]:





# In[2]:


from typing import List
import numpy as np
import sys

def matmul(A: List[List[int]], B: List[List[int]]) -> List[List[int]]:
    n = len(A)
    C = [[0 for j in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            for k in range(n):
                C[i][j] += A[i][k] * B[k][j]
    return C

A = [[1, 2], [3, 4]]
B = [[5, 6], [7, 8]]
C = matmul(A, B)
print(C)


# In[ ]:





# In[11]:


#1-b


# In[12]:




import numpy as np
import sys
import argparse
import time
import matplotlib.pyplot as plt


# In[ ]:


import argparse
import numpy as np
import sys

def matmul(A, B):
    n = len(A)
    C = [[0 for j in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            for k in range(n):
                C[i][j] += A[i][k] * B[k][j]
    return C

# Define command line argument
parser = argparse.ArgumentParser(description='Process an integer input.')
parser.add_argument('input', type=int, help='an integer input of either 0 or 1')
args = parser.parse_args()

# Set plotBool based on input
if args.input == 1:
    plotBool = True
elif args.input == 0:
    plotBool = False
else:
    raise ValueError("Input must be either 0 or 1.")

# Perform matrix multiplication and print result if plotBool is True
if plotBool:
    A = [[1, 2], [3, 4]]
    B = [[5, 6], [7, 8]]
    C = matmul(A, B)
    print(C)


# In[ ]:





# In[17]:


#1-c


# In[ ]:


import argparse
import numpy as np
import random
import time

def matmul(A, B):
    n = len(A)
    C = [[0 for j in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            for k in range(n):
                C[i][j] += A[i][k] * B[k][j]
    return C

# Define command line argument
parser = argparse.ArgumentParser(description='Process an integer input.')
parser.add_argument('input', type=int, help='an integer input of either 0 or 1')
args = parser.parse_args()

# Set plotBool based on input
if args.input == 1:
    plotBool = True
elif args.input == 0:
    plotBool = False
else:
    raise ValueError("Input must be either 0 or 1.")

n = 100
# Create 2D lists A and B of size n x n
A_list = [[random.uniform(-1, 1) for j in range(n)] for i in range(n)]
B_list = [[random.uniform(-1, 1) for j in range(n)] for i in range(n)]

# Create numpy arrays A and B with same elements and size
A = np.array(A_list)
B = np.array(B_list)

# Perform matrix multiplication and print result if plotBool is True
if plotBool:
    start_time = time.time()
    C = matmul(A_list, B_list)
    print("Python list time:", time.time() - start_time)
    
    start_time = time.time()
    C_np = np.matmul(A, B)
    print("NumPy time:", time.time() - start_time)


# In[ ]:





# In[36]:


#1-f


# In[39]:


import time
import numpy as np
import matplotlib.pyplot as plt


plotBool = True

if plotBool:
    n_values = [2**5, 2**6, 2**7, 2**8]
    matmul_times = []
    numpy_times = []

    for n in n_values:
        # Using the matmul function
        A = [[i * n + j + 1 for j in range(n)] for i in range(n)]
        B = [[i * n + j + 1 for j in range(n)] for i in range(n)]
        start_time = time.perf_counter()
        C = matmul(A, B)
        end_time = time.perf_counter()
        elapsed_time = (end_time - start_time) * 1000
        matmul_times.append(elapsed_time)

        # Using numpy arrays and @ operator
        A = np.array([[i * n + j + 1 for j in range(n)] for i in range(n)])
        B = np.array([[i * n + j + 1 for j in range(n)] for i in range(n)])
        start_time = time.perf_counter()
        C = A @ B
        end_time = time.perf_counter()
        elapsed_time = (end_time - start_time) * 1000
        numpy_times.append(elapsed_time)

    # Create log-log plot
    plt.loglog(n_values, matmul_times, label="matmul")
    plt.loglog(n_values, numpy_times, label="numpy")
    plt.xlabel("n (log base 2)")
    plt.ylabel("Time (ms) (log base 10)")
    plt.title("Comparison of Matrix Multiplication Methods")
    plt.legend()
    plt.show()


# In[ ]:

#1-d


# In[ ]:


import time


# Define a list of values of n to test
n_values = [2**5, 2**6, 2**7, 2**8]

# Loop over the values of n and measure the time taken by the matmul function
for n in n_values:
# Define two random matrices A and B of size n x n
A = [[i + j for j in range(n)] for i in range(n)]
B = [[i - j for j in range(n)] for i in range(n)]

# Measure the time taken by the matmul function to multiply A and B
start_time = time.perf_counter()
C = matmul(A, B)
end_time = time.perf_counter()
elapsed_time = (end_time - start_time) * 1000.0  # convert to milliseconds

# Print the size of the matrices and the time taken by the matmul function
print(f"n = {n}: {elapsed_time:.6f} ms")


# In[ ]:





# In[ ]:


import numpy as np
import time

# Define a list of values of n to test
n_values = [2**5, 2**6, 2**7, 2**8]

# Loop over the values of n and measure the time taken by the @ operator
for n in n_values:
    # Define two random matrices A and B of size n x n as NumPy arrays
    A = np.random.rand(n, n)
    B = np.random.rand(n, n)
    
    # Measure the time taken by the @ operator to multiply A and B
    start_time = time.perf_counter()
    C = A @ B
    end_time = time.perf_counter()
    elapsed_time = (end_time - start_time) * 1000.0  # convert to milliseconds
    
    # Print the size of the matrices and the time taken by the @ operator
    print(f"n = {n}: {elapsed_time:.6f} ms")


# In[ ]:





# In[2]:


#1-e


# In[ ]:


import numpy as np
import time

# Define a list of values of n to test
n_values = [2**5, 2**6, 2**7, 2**8]

# Loop over the values of n and measure the time taken by the @ operator
for n in n_values:
    # Define two random matrices A and B of size n x n as NumPy arrays
    A = np.random.rand(n, n)
    B = np.random.rand(n, n)
    
    # Measure the time taken by the @ operator to multiply A and B and get the first element of the resultant matrix C
    start_time = time.perf_counter()
    C = A @ B
    end_time = time.perf_counter()
    elapsed_time = (end_time - start_time) * 1000.0  # convert to milliseconds
    first_element = C[0][0]
    
    # Print the size of the matrices, the time taken by the @ operator, and the first element of C
    print(f"n = {n}: time = {elapsed_time:.6f} ms, first element of C = {first_element:.6f}")


# In[ ]:





# In[ ]:


import time
from pyMatmul import matmul

n = 256

# Using the matmul function
A = [[i * n + j + 1 for j in range(n)] for i in range(n)]
B = [[i * n + j + 1 for j in range(n)] for i in range(n)]
start_time = time.perf_counter()
C = matmul(A, B)
end_time = time.perf_counter()
elapsed_time = (end_time - start_time) * 1000
print(f"Using matmul function: elapsed time = {elapsed_time:.3f} ms")


# In[ ]:





# In[ ]:


import time
import numpy as np

n = 256

# Using numpy arrays and @ operator
A = np.array([[i * n + j + 1 for j in range(n)] for i in range(n)])
B = np.array([[i * n + j + 1 for j in range(n)] for i in range(n)])
start_time = time.perf_counter()
C = A @ B
end_time = time.perf_counter()
elapsed_time = (end_time - start_time) * 1000
print(f"Using numpy arrays and @ operator: elapsed time = {elapsed_time:.3f} ms")




#For the matmul function




import time


n = 256

# Using the matmul function
A = [[i * n + j + 1 for j in range(n)] for i in range(n)]
B = [[i * n + j + 1 for j in range(n)] for i in range(n)]
start_time = time.perf_counter()
C = matmul(A, B)
end_time = time.perf_counter()
elapsed_time = (end_time - start_time) * 1000
print(f"Using matmul function: elapsed time = {elapsed_time:.3f} ms")
print(f"First element of the resultant matrix = {C[0][0]}")



#For the @ operator:





import time
import numpy as np

n = 256

# Using numpy arrays and @ operator
A = np.array([[i * n + j + 1 for j in range(n)] for i in range(n)])
B = np.array([[i * n + j + 1 for j in range(n)] for i in range(n)])
start_time = time.perf_counter()
C = A @ B
end_time = time.perf_counter()
elapsed_time = (end_time - start_time) * 1000
print(f"Using numpy arrays and @ operator: elapsed time = {elapsed_time:.3f} ms")
print(f"First element of the resultant matrix = {C[0][0]}")


