#!/usr/bin/env python
# coding: utf-8

# In[1]:


class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        
    def __hash__(self):
        return hash(self.name)
    
    def __dir__(self):
        attrs = set(dir(type(self)))
        attrs.update(self.__dict__)
        return list(attrs)

p1 = Person("Alice", 30)
p2 = Person("Bob", 40)

print(hash(p1))   # prints the hash value of p1's name
print(hash(p2))   # prints the hash value of p2's name

print(dir(p1))    # prints the valid attributes for p1
print(dir(p2))    # prints the valid attributes for p2


# In[ ]:




